# Instructions for Internship Admins

Internship Admins (often refered as "Org admins") are members of the [Internship Committee](https://wiki.gnome.org/InternshipCommittee) responsible for coordinating GNOME's participation in internship programs such as Google Summer of Code and Outreachy.

This guide describes in chronological order the tasks required by internship admins. It also includes a collection of message/email templates that can be used to communicate with organizations, mentors, and interns.

## Considerations

- The timeline needs to be adapted yearly, based on the schedule of of the internship programs. It is also important to stay up to date to the rules of the programs.

- Both GSoC and Outreachy tend to send informative emails about each important date on their program schedule.

- GSoC currently runs once a year (during the northern hemisphere summer) while Outreachy runs twice a year.

## Summary of responsibilities

- Apply on behalf of GNOME to participate in internship programs
- Answer questions participants might have about the internships
- Mediate conflicts and disagreements between participants
- Publicize GNOME's internship opportunities and results

## Timeline

### 1. Nurture the repository of project ideas

[https://gitlab.gnome.org/Teams/Engagement/internship-project-ideas](https://gitlab.gnome.org/Teams/Engagement/internship-project-ideas)

Internship Admins should incentivize contributors to propose project ideas all year long. This way project proposals are mostly ready when the call for project ideas for the programs takes place.

> Historically we have had a couple of last minute proposals that failed to scope the work tasks or have failed to consider its impact and usefulness to other components, causing internship projects to be set for failure right from the start. Therefore, it is the duty of the admins to review proposals and consult with stakeholders on the affected projects.

**For Google Summer of Code, organizations are required to have a preliminary list of project ideas available by the time the organization applies to participate in the program. Therefore is important to have at least a preliminary list of project ideas before filling the application form for GSoC.**

### 2. Identify the organization application dates

Monitor the [GSoC](https://summerofcode.withgoogle.com) and [Outreachy](https://www.outreachy.org/) websites and social media accounts to identify the dates for organizations to apply. The organization application dates for GSoC and Outreachy tend to be at the beginning of the calendar year.

### 3. Make a "Call for Mentors and Project Ideas"

For Google Summer of Code organizations are required to have a preliminary list of project ideas available by the time the organization applies to participate in the program. Therefore is important to publish at least a draft of [https://gsoc.gnome.org/$YEAR]() before filling the application form.

For this reason, as soon as the GSoC dates are announced, Internship Admins should [post a "Call for Mentors and Project Ideas" to our Discourse community](https://discourse.gnome.org/c/community/outreach/). This announcement should be publicized within our community channels (Planet GNOME, This Week in GNOME, GNOME Hackers chat room, etc...).

- [TEMPLATE] [[GSOC] Call for Mentors and Project Ideas" message template](templates/gsoc-call-for-mentors-and-project-ideas.md).
- [TEMPLATE] [[Outreachy] Call for Mentors and Project Ideas" message template](templates/outreachy-call-for-mentors-and-project-ideas.md).

### 4. Request sponsorship from the Board for Outreachy

Outreachy internship's funding comes from the mentoring organizations. Therefore we need to message the GNOME Foundation Board of Directors and request funding for the respective Outreachy cohort. Consult in the Outreachy website for the current internship costs and with the Board to decide on how many internships the organization is willing to sponsor.

### 5. Apply on behalf of GNOME to participate on the programs

When is time to complete the application forms, check the templates for the programs:

- [TEMPLATE] [Application form answers for Google Summer of Code](templates/gsoc-organization-application-form.md)
- [TEMPLATE] [Application form answers for Outreachy](templates/outreachy-organization-application-form.md)

> For Outreachy, you can complete the form and mark the internship funding as "tentative" before you get an answer/approval from the Board.

### 6. Submit Outreachy project ideas to their website

After obtaining the number of slots the Board is willing to sponsor, it is time to decide which projects are moving forward.

**The [Internship Committee](https://wiki.gnome.org/InternshipCommittee) is responsible for establishing a process to select the internship projects which are most relevant to GNOME (worth funding).**

After deciding which projects are accepted, Internship Admins need to tell mentors to submit project proposals to the Outreachy website (which will also be reviewed by Outreachy organizers).

> Outreachy expects mentors to submit project proposals themselves, not admins.

### 7. Announce GNOME participation in GSoC

When we receive confirmation that we are accepted, it is time to announce to the broad community/public.

The purpose of this announcement is to let potential interns know that GNOME will be part of the program, so that they start making their first contributions, discussing project ideas with mentors, and working on their individual internship application.

- [TEMPLATE] [Announcement of GNOME participation in GSoC](gsoc-announcement-of-participation.md)

### 7. Review project applications

**For Google Summer of Code**, after the GSoC "contributor application period" ends, it is time for Internship Admins to rank proposals.

> Ranking proposals will indicate to Google which projects are more interesting to our community. For example, if we rank 10 proposals, we are asking Google for 10 project slots. If Google grants us 8 projects slots, we will automatically receive ranked proposals #1 to #8.

**For Outreachy**, we need to discuss with mentors and then visit their website and select which iternship application(s) we are moving forward with (as the number of slots is defined by how many internships we have budget to sponsor).

### 8. Announce the selected projects

Once Google or Outreachy confirm the list of selected projects, admins should publish an announcement to let the broad community know about the projects we are moving forward with.

- [TEMPLATE] [Announcement of selected projects for GSoC](templates/gsoc-announcement-of-selected-projects.md)
- [TEMPLATE] [Announcement of selected projects for Outreachy](templates/outreachy-announcement-of-selected-projects.md)

### 9. Start the onboarding process of interns

You can obtain the email addresses of the participants on the respective program's website.

Message all interns with their initial onboard instructions.

- [TEMPLATE] [Onboarding instructions for interns](templates/onboarding-interns.md)

### 10. Organize community bonding activities

This is the ideal time to schedule a call with interns, organize a workshop, etc... as well as the time to help interns setup their accounts and communication channels.

> It is important to ensure at this point that there's a frequent communication between interns and their respective mentors. The internship programs often have a mechanism to remove an inactive participant at this stage of the process (consult with their terms and conditions).

### 11. Invite interns to attend GUADEC

An important part of the internship experience with GNOME is the interns' participation at GUADEC.

The GNOME Foundation, through the [Travel Committee](https://wiki.gnome.org/Travel/Committee), allocates part of the travel budget to fund the travel and accommodation of our current interns. Interns are expected to apply for travel sponsorship if needed, and start planning their GUADEC participation.

Admins should monitor the Travel Committee communication channels for when they announce that they are open to receive GUADEC travel sponsorship requests.

> Interns should be encouraged to participate and even organize BoF/Hackfests with their mentors and other contributors.

Interns are also invited to present a brief 5 minutes lightning talk about the project during our "GNOME Intern Lightning Talks Session". See the guide for [Organizing the Intern Lightining Talks session](guadec-intern-lightning-talks.md) for more information.

- [TEMPLATE] [Template invitation to GUADEC](templates/guadec-invite.md)

Often interns will have questions and doubts about travel, therefore admins should be available to answer questions and help interns book their trip.

> We often have to provide an invitation letter for interns to travel for Visa reasons. The local organization for GUADEC is usually the one that is expected to provide visa invitation letters since those should include a local point of contact and additional information about the trip and conference.

### 12. Supervise the evaluations

Mentors are expected to evaluate the progress of the interns during certain parts (milestones) of the program. Internship Admins are expected to supervise this process and make sure everything is going as expected within the timeline.

> **IMPORTANT:** Internship programs can often "punish" an organization for failing to meet its important deadlines.

### 13. Wrap the Internship cohort

Once a season of GSoC or a cohort for Outreachy has ended, Internship Admins should publicize the results of our interns to the public and broad community.

An announcement for each program ending could be posted to our Discourse community and also shared as a blog post in Planet GNOME.

- [TEMPLATE] [Announcement of GSoC/Outreachy Results](templates/final-results-announcement.md)

### 14. Work on alumni retention

One of our goals with the internship programs is to provide an opportunity for interns to become long term contributors to our project, and even future mentors.

> We have had interns that were so successful that by the end of their internship they have established themselves as maintainers/co-maintainers of the projects they worked on. 

Contributors should be encouraged to stay engaged with the project after the internship. If they do so, we should conduct them towards the path of [GNOME Foundation membership](https://foundation.gnome.org/membership/).

Internship Admins can help interns stay engaged by promoting their work within potential employers involved or related to the ecosystem.

- [ARTICLE] [Tips for interns after their internship ends](https://feborg.es/summertime-sadness/)