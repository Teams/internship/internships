# GUADEC Interns Lightning Talks Session

This is a guide describing what needs to be done to organize a GNOME Interns Lightning Talks Session.

Internship Admins need to monitor news about GUADEC to identify when to organize the lightning talk sessions and set its talk submissions deadline.

For convenience, we merge all PDFs from the interns into a single one, so that they get to present one after the other without interruptions.

For interns that aren't participating physically at the event, admins should consult with the local organizers for whether a remote presentation or a pre-recorded video is the best approach.

A Internship Admin member should host the event and be the main point of contact with the GUADEC organization team responsible for the schedule. A member should also provide a quick introduction to our internship efforts and coordinate the presentations of the interns.

# Invite Interns

The message below should be sent to all interns by email.

## Variables

- **$GUADEC_YEAR:** For example: "2024"
- **$GUADEC_REGISTRATION_URL**: The link for the event at https://events.gnome.org
- **$$TALK_SUBMISSION_DEADLINE:** Admins should establish a deadline that gives them enough time to prepare the session.

## Template

> # [ACTION-REQUIRED] GUADEC $GUADEC_YEAR Interns Lightning Talks session
> 
> Hi folks,
> 
> TL;DR: reply to this email letting us know whether you are planning to attend GUADEC *physically or remotely, and if you have any issues with presenting a 5-minute lightning talk.
> 
> I hope you are enjoying your internship time and learning plenty from your mentor(s).
> 
> You have received an email from me with the subject "Apply for GUADEC travel sponsorship ASAP!" and hopefully you can make it to GUADEC to meet the GNOME community. If you can't, that's fine, the conference organizers will do their best to make sure that the remote experience is great.
> 
> Don't forget to register as an attendee!
> $GUADEC_REGISTRATION_URL
> 
> During GUADEC we traditionally hold our "Intern Lightning Talks" session, which is a slot in the schedule when each one of our interns gets 5 minutes to present a quick sneak peak on themselves and their internship project.
> 
> Here are the recordings of previously held lightning talk sessions for reference: https://www.youtube.com/watch?v=8jBrQFBjmlw and https://www.youtube.com/watch?v=VadiNFq26e8
> 
> Please, start working on your slides (usually 3 to 5 pages are sufficient). In your presentation you will introduce yourself, mention your mentor, speak briefly about your internship project, and mention some future plans/follow-ups if you have).
> 
> * Send your slides as PDF to soc-admins@gnome.org before $TALK_SUBMISSION_DEADLINE (we will send reminder emails).
> 
> Please, reply to this email letting us know whether you are planning to attend GUADEC *physically or remotely, and if you have any issues with presenting a lightning talk. If you are attending it remotely, please pre-record your lightning talk and send me the video before $TALK_SUBMISSION_DEADLINE.
> 
> If you have any questions, feel free to reply to this message, send an email to the GSoC administrators (soc-admins@gnome.org), or ask your mentor.
> 
> Cheers,
> GNOME GSoC Administrators
