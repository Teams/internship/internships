# Final results announcement


# Variables

- **$NUMBER_OF_INTERNS:** The number of interns for this program season.
- **$PROGRAM_NAME:** "Outreachy" or "Google Summer of Code"
- **$PROJECTS_SUMMARY:** For example "improving apps in our ecosystem to standardizing our web presence**

# Template

> Another program year is ending and we are extremely happy with the resulting work of our contributors!
>  
> This year GNOME had $NUMBER_OF_INTERNS $PROGRAM_NAME projects covering various areas, from $PROJECTS_SUMMARY. We hope our interns had a glimpse of our community that motivated them to continue engaged with their projects and involved with the broad GNOME ecosystem.
>  
> A special thanks goes to our mentors that are the front-line of this initiative, sharing their knowledge and introducing our community to the new contributors. Thank you so much!
> 
> We encourage interns now to contemplate their future after $PROGRAM_NAME. If you want to continue with us, speak to your mentor about your interests and ask for some tips on how you can continue participating in the project. Also, there are opportunities of employment that can help you build a career in open source.
> 
> Thanks for choosing GNOME for your internship! We were lucky to have you!

