# Announcement of GNOME Participation in Google Summer of Code

This message should be posted in [our Discourse community for internships](https://discourse.gnome.org/c/community/outreach) and shared broadly to the general public.

# Variables

- **$GSOC_YEAR:** For example:** "2024".
- **$LIST_OF_IMPORTANT_DATES:** This list should be a summary of the important dates from the start of the GSoC timeline to the "Coding Starts" date.

# Template

> # Applications for Google Summer of Code $GSOC_YEAR with GNOME are now open!
> 
> We are glad to announce that once again the GNOME Foundation will be part of Google Summer of Code. We are interested in onboarding new contributors that are passionate about GNOME and motivated to become long term GNOME developers!
> 
> ## Important information
> 
> - **For Contributors** interested in participating in GSoC with GNOME should visit https://gsoc.gnome.org for more information.
> - **For Mentors** interested in mentoring projects this year should file a gitlab issue in https://gitlab.gnome.org/Teams/Engagement/internship-project-ideas/-/issues
> 
> # Important upcoming dates:
> 
> $LIST_OF_IMPORTANT_DATES
> 
> For more information, visit the [Google Summer of Code timeline](https://developers.google.com/open-source/gsoc/timeline).
> 
> If you have any doubts or questions, please reply to this message on Discourse or privately at soc-admins@gnome.org
