# Announcement of GNOME selected projects for GSoC

This message should be posted in [our Discourse community for internships](https://discourse.gnome.org/c/community/outreach) and shared broadly to the general public.

## Variables
- **$GSOC_YEAR:** For example:** "2024".
- **$NUMBER_OF_GSOC_PROJECTS:** How many interns we will be mentoring in this season.
- **$LIST_OF_PROJECTS:** This is often better presented with a table containing columns for: Project Title, Contributor and Assigned Mentor(s). See https://discourse.gnome.org/t/announcement-gnome-will-be-mentoring-9-new-contributors-in-google-summer-of-code-2023/15232 for example.
- **$GSOC_COMMUNITY_BONDING_PERIOD**: This is a range of dates describing the community bonding period. This information can be obtained on this year's [GSoC timeline](https://developers.google.com/open-source/gsoc/timeline) page. For example: "May 4th to 28th".
- **GSOC_CODING_START_DATE:**: This can also be obtained at the [GSoC timeline](https://developers.google.com/open-source/gsoc/timeline) page.

## Template

> # Announcement: GNOME will be mentoring 9 new contributors in Google Summer of Code $GSOC_YEAR!
  >
> We are happy to announce that GNOME was assigned nine slots for Google Summer of Code $NUMER_OF_GSOC_PROJECTS projects this year!
> 
> GSoC is a program focused on bringing new contributors into open source software development. A number of long term GNOME developers are former GSoC interns, making the program a very valuable entry point for new members in our project.
> 
> In $GSOC_YEAR we will mentoring the following projects:
> 
> $LIST_OF_PROJECTS
> 
> As part of the contributor’s acceptance into GSoC they are expected to actively participate in the [Community Bonding period $GSOC_COMMUNITY_BONDING_PERIOD](https://developers.google.com/open-source/gsoc/timeline). The Community Bonding period is intended to help prepare contributors to start contributing at full speed starting $GSOC_CODING_START_DATE.
> 
> The new contributors will soon get their blogs added to [Planet GNOME](https://planet.gnome.org) making it easy for the GNOME community to get to know them and the projects that they will be working on.
> 
> We would like to also thank our mentors for supporting GSoC and helping new contributors enter our project.
> 
> If you have any doubts, feel free to reply to this Discourse topic or message us privately at soc-admins@gnome.org