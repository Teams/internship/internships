# Google Summer of Code: Call for Mentors and Project Ideas

This message should be posted in [our Discourse community for internships](https://discourse.gnome.org/c/community/outreach) and shared within our community channels, such as Planet GNOME and GNOME Hackers.

## Variables

- **$SUBMISSION_DEADLINE_DATE:** The last day for mentors to submit project proposals. For example: "Sept 20, 2023".
- **$GSOC_YEAR:** This is the year of the program, for example: "2024".

## Template

> # [Deadline $SUBMISSION_DEADLINE_DATE] Call for Project ideas and mentors for Google Summer of Code $GSOC_YEAR
  >
> It is that time of the year again when we start gathering ideas and mentors for [Google Summer Code](https://summerofcode.withgoogle.com/).
> 
> Please, submit your project ideas as issues in our [Project ideas GitLab repository](https://gitlab.gnome.org/Teams/Engagement/internship-project-ideas/). Make sure you answer all the questions in the issue template (Project-Proposal template).
> 
> The GNOME Foundation recognizes that mentoring is a time consuming effort, and for this reason, we will be giving accepted mentors an option to [receive the $500 USD stipend that Google pays the organization for each contributor](https://summerofcode.withgoogle.com/rules). Mentors can choose to revert the fund into a donation to the GNOME Foundation. Some payment restrictions may apply (please contact us for questions).
> 
> Proposals will be reviewed by the GNOME GSoC Admins and posted in our [Project Ideas page for GSoC $GSOC_YEAR](https://gsoc.gnome.org/$GSOC_YEAR).
> 
> If you have any doubts, please don’t hesitate to contact the GNOME GSoC Admins on this very same forum, over email at soc-admins@gnome.org, or on our Matrix channel #soc:gnome.org