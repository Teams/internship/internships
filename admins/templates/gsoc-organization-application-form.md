# GSoC: Organization Application form

The Internship Committee should meet and reword these answers if they find it appropriate.

## Organization Profile

- **Website URL for Organization**: https://gnome.org
- **Logo:** Make sure to obtain one at https://brand.gnome.org/
- **Tagline (50 characters maximum):** A diverse and sustainable free software desktop.
- **Primary Open Source License:**: GNU General Public License Version 2 (GPL-2.0)
- **What year was your project started:** 1999
- **Link to your source code location:** https://gitlab.gnome.org
- **Organization Categories (You may select up to 2 categories):** "End User Applications" and "Operating Systems".
- **Ideas List**: https://gsoc.gnome.org/$GSOC_YEAR

> This is the most important information of this application. Google will evaluate the ideas list page to decide whether to accept our organization in GSoC. This list should already exist by the time we apply. Google also doesn't like links to unfiltered bugtrackers or other specialized tools.
> 
> Prospective GSoC Contributors will view this link on the GSoC websites.

- **Organization Technologies (up to 5)**: C, Linux, Rust, GTK, Flatpak
- **Organization Topics (up to 5)**: Enter keywords for general topics that describe our organization. Examples: desktop, operating systems, graphics, open source, apps.
- **Organization Description:**

The GNOME Foundation is a non-profit organization that believes in a world where everyone is empowered by technology they can trust. We do this by building a diverse and sustainable free software personal computing ecosystem.

- **Contributor Guidance:** https://gsoc.gnome.org

> Provide potential contributors with a page containing tips on how to write a successful proposal.

- **Direct Communication Methods:**

  - Chat: https://handbook.gnome.org/get-in-touch/matrix.html
  - Mailing list/Forum: https://discourse.gnome.org/

- **Social Communication Methods:**

    - Social Media - Twitter: https://twitter.com/gnome
    - Blogs: https://planet.gnome.org

- **Signed Agreements**: Read and sign the agreements.

## Organization Questionnaire

- **Why does your organization want to participate in Google Summer of Code?**

We would like to help contributors gain a good understanding and appreciation for the values of working on open source by participating in an active community such as ours. We would like to encourage contributors to become long term contributing community members. Many active members of our community were at one time GSoC students.

- **What would your organization consider to be a successful GSoC Program?**

A successful GSoC program is when our contributors feel welcomed in our community and have the desire to continue participating in it after the program. Part of our community is made of former GSoC participants, and this is a proof that we are successfully retaining GSoC talent.

An ideal contributor is would to take ownership over part of the project that they worked on, and continue contributing to the point where they could obtain the "maintainer" role.

- **How will you keep mentors engaged with their GSoC contributors?**

Many of our mentors have prior experience mentoring for GSoC and Outreachy so know what is expected of them. New mentors will always have a more experienced mentor guiding them. In addition, the admin team provide ongoing support and assistance to mentors who need it. We expect mentors to communicate regularly with their students. The admin team will check in with both students and mentors throughout the internship programme.

- **How will you keep your GSoC contributors on schedule to complete their projects?**

Firstly, before GSoC even starts, the contributors work closely with their mentors to come up with a realistic schedule which fits within the ability of the contributor and the scope of the project. Our mentors talk to contributors every day to check in on their progress and guide them as needed. Contributors report publicly to our community via blog posts (planet.gnome.org) on a regular basis as part of their internships. If a contributor misses their report, the admin team will follow up with them and the mentor to check that there are no problems.

- **How will you get your GSoC contributors involved in your community during GSoC?**

Potential contributors will need to start interacting with the community during the application period to submit a patch as per our application requirements. During the community bonding period, we will make sure that contributors start following GNOME's communication channels channels and talking to other members of the community. Our contributors will have to work with other teams within GNOME and with people who are not their mentors as our projects are rarely isolated. Contributors will report to our community on a weekly basis using blog posts. Lastly, and most importantly, we make sure that our contributors can attend at least one event, usually GUADEC, our largest contributor conference, during their internships - we do provide financial assistance to help them. We also encourage contributors to organize a hackfest either as part of GUADEC or separately from it.

- **Anything else we should know? (Optional)**

We love GSoC and it has helped our community to bring new members many years (our team of mentors and admins is composed of mostly former-gsoc students).

- **Is your organization part of any government?** No.

- **GSoC Reference Contact (Optional)**: Leave it blank (we are one of the old GSoC organizations in the program).
