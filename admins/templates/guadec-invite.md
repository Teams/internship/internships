# Invitation to GUADEC

This message should be posted on our Discourse community and sent directly to the interns on their email addresses. The intern's email addresses can be obtained from the GSoC/Outreachy websites.

> You can cc soc-admins@gnome.org and copy all intern emails as cco.

# Variables

- **GUADEC_LOCATION**: For example, "Riga, Latvia".
- **GUADEC_DATES**: For example, "July 26th to July 31st"

# Template

> # Apply for GUADEC travel sponsorship ASAP!
> 
> Hi folks,
> 
> I hope your first GSoC week is going well, with you joining the proper channels, chatting with your mentor(s), and setting up a blog for your weekly reports. Make sure you haven't missed our onboarding instructions.
>
> This email is to invite you to participate in GUADEC, the main GNOME community conference that happens every year. It is taking place this year in $GUADEC_LOCATION from $GUADEC_DATES. See https://guadec.org for more info.
> 
> This is a great place for you to meet a big part of the GNOME community in person, attend talks, hackfests and workshops. It is also a great chance to meet your mentor(s).
> 
> The GNOME Foundation has a budget to help sponsor the travel for our Foundation members and GSoC/Outreachy interns. So if you are interested in attending GUADEC, start planning as soon as possible!
> 
> Firstly, send a travel request to the GNOME travel committee. Visit $GUADEC_TRAVELS_SPONSORSHIP_LINK. Mention that you are an intern in your travel request application.
> 
> Secondly, make sure you have all the travel documents you need. Some countries require Visa applications, invitation letters, etc... and this type of thing takes time. So hurry up!
> 
> If you have any doubts or issues, reply to this email or open a topic under the #gsoc tag in https://discourse.gnome.org/c/community/outreach/334
> 
> Cheers,
> Felipe Borges.

