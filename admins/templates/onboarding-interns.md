# Onboarding Interns

This message should be posted on our Discourse community and sent directly to the interns on their email addresses. The intern's email addresses can be obtained from the GSoC/Outreachy website.

> You can cc soc-admins@gnome.org and copy all intern emails as cco.

# Variables

- **$PROGRAM_NAME:** Example "Google Summer of Code" or "Outreachy".

# Template

> # Welcome to GSoC with GNOME! Here's how to get started!
> 
> Congratulations: you did it! The GNOME community is glad to welcome you, and to have you participate in $PROGRAM_NAME and work on GNOME!
> 
> Some of you already know the GNOME community quite well, but it might not be the case for every one of you. That's not a problem: GSoC has a community bonding period, where you will have time to get to know the community before starting the real work. If you are into history, this is a great video about the history of GNOME https://www.youtube.com/watch?v=bWUmptI6O2w
> 
> If during the GSoC, you have a problem with your mentor (lack of communication, or misunderstandings, or deep disagreements, or anything else), don't hesitate to contact the GNOME administrators (soc-admins@gnome.org).
> 
> Firstly, we suggest you contact your mentor and subscribe to our #gsoc tag in Discourse https://discourse.gnome.org/tag/gsoc (click the bell icon on the top right). And then proceed to introduce yourself at our "Say Hello" thread at https://discourse.gnome.org/t/say-hello-thread/82 so other members of the community have a chance to get to know you, your mentor, and your project.
> 
> Planet GNOME is an aggregator of blogs of GNOME contributors. From now on, it will serve as your morning newspaper :-). We ask that you add your blog to Planet GNOME, and write updates about your work during the internship period *every two weeks*. Of course, you are welcome to blog more often too! It is exciting that you will have a voice on Planet GNOME right from the start and it is a very important way to let the GNOME community know about the work you are doing! This will also allow us to keep track of your progress. Once your blog is added, please write an introductory post and start blogging about your work on GNOME and other things that you see relevant for Planet GNOME. https://planet.gnome.org
> 
> To get your blog added to Planet GNOME, please follow the instructions at https://wiki.gnome.org/PlanetGnome (no worries if you don't want to have a photo).
> 
> Secondly, you should join the "GNOME Hackers" chat room https://matrix.to/#/#gnome-hackers:gnome.org and our "GNOME GSoC" chat  room https://matrix.to/#/#soc:gnome.org on Matrix. If the project that you are working on has dedicated channels, make sure you join those too (ask your mentor). If you have any doubts about our chatting platforms, check https://wiki.gnome.org/Community/GettingInTouch/IRC
> 
> That's all for now. If you have any questions, feel free to send an email to the administrators, ask your mentor, or write on our Discourse under the #soc tag.
> 
> Small summary:
> 
> - Make sure you're in contact with your mentor(s)
> - Subscribe to our Discourse tag to receive news and updates about your internship
> - Get your blog added to Planet GNOME
> - Publish an introductory blog post about yourself and your project
> - Join the relevant chat channels
> - Get to know the GNOME community
> - Get ready to start coding!
> 
> Welcome and Congratulations!
> 
> Cheers,
> GNOME GSoC Administrators

