# Announcement of GNOME selected projects for Outreachy

This message should be posted in [our Discourse community for internships](https://discourse.gnome.org/c/community/outreach) and shared broadly to the general public.

> The importance of this message is to let our community (and donors) know that GNOME is using its community funds to fund Outreachy interns, which is aligned with our community's diversity and inclusion goals.

We often have less Outreachy interns than GSoC interns, so this message don't require a table to display the project lists like the GSoC one.

## Variables
- **$NUMBER_OF_OUTREACHY_PROJECTS:** How many interns we will be mentoring in this season.
- **$OUTREACHY_COHORT_PERIOD:** This is the name of the cohort period. Often (December-March) or (May-August).
- **$OUTREACHY_INTERNS_NAMES:** A list of names, comma separated. For example: "Felipe Borges and Sonny Piers."
- **$OUTREACHY_INTERN_1_NAME**: This is the name of the first intern. This list should have an entry for each intern.
- **$OUTREACH_INTERN_BLOG_OR_GITLAB:** the URL for their respective blog or GitLab profile.
- **OUTREACHY_MENTOR_NAME:** For example, "Felipe Borges".
- **OUTREACHY_MENTOR_BLOG_OR_GITLAB:** Similar to the intern variable.

## Template

> # Announcement: $NUMBER_OF_OUTREACHY_PROJECTS GNOME projects selected for the $OUTREACHY_COHORT_PERIOD Outreachy cohort
> 
> We are happy to announce that the GNOME Project is sponsoring $NUMBER_OF_OUTREACHY_PROJECTS [Outreachy ](https://outreachy.org) projects for the $OUTREACHY_COHORT_PERIOD Outreachy cohort.

> Let’s welcome $OUTRECHY_INTERNS_NAMES
> 
> 
> * [$OUTREACHY_INTERN_1_NAME]($OUTREACHY_INTERN_BLOG_OR_GITLAB) working with your mentor [$OUTREACHY_MENTOR_NAME]($OUTREACHY_MENTOR_BLOG_OR_GITLAB) on the project "$PROJECT_TITLE"
> 
> The new contributors will soon get their blogs added to [Planet GNOME](https://planet.gnome.org) making it easy for the GNOME community to get to know them and the projects that they will be working on.
 >  
 > We would like to also thank our mentors for supporting Outreachy and helping new contributors enter our project.
 > 
 > If you have any doubts, feel free to reply to this Discourse topic or message us privately at soc-admins@gnome.org
