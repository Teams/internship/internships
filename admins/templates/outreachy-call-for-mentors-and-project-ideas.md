# Outreachy: Call for Mentors and Project Ideas

This message should be posted in [our Discourse community for internships](https://discourse.gnome.org/c/community/outreach) and shared within our community channels, such as Planet GNOME and GNOME Hackers.

## Variables

- **$SUBMISSION_DEADLINE_DATE:** The last day for mentors to submit project proposals. For example: "Sept 20, 2023".
- **$OUTREACHY_COHORT_PERIOD:** This is the name of the cohort period. Often (December-March) or (May-August).
- **$NUMBER_OF_SPONSORED_INTERNS:** This describes the number of interns the Board has a budget available to sponsor for this given Outreachy cohort.

- **$OUTREACHY_GNOME_PROJECT_SUBMISSION_PAGE:** Once our organization is accepted in Outreachy, we can obtain an URL for mentors to submit project proposals. This URL changes for every Outreachy cohort. For example, in the past this URL looked like this: https://www.outreachy.org/outreachy-december-2023-internship-round/communities/gnome/submit-project/

## Template

> # [Deadline $SUBMISSION_DEADLINE_DATE] Call for Mentors for Outreachy $OUTREACHY_COHORT_PERIOD cohort
  >
>The GNOME Foundation is interested in sponsoring up to $NUMBER_OF_SPONSORED_INTERNS Outreachy interns for the $OUTREACHY_COHORT_PERIOD cohort.
> 
> If you are interested in mentoring AND have a project idea in mind, visit GNOME: [Call for Outreachy mentors and volunteers]($OUTREACHY_GNOME_PROJECT_SUBMISSION_PAGE) and submit your proposal..
> 
> We can also use pre-submitted ideas from our Internship project ideas repository 22.
  >
> GNOME has a committee (Allan Day, Matthias Clasen and Sri Ramkrishna) that will triage projects before approval. More information about GNOME’s participation in Outreachy is available at https://wiki.gnome.org/Outreach/Outreachy
  >
> If you have any questions, please feel free to reply to this thread or e-mail soc-admins@gnome.org, which is a private mailing list with the GNOME internship coordinators.