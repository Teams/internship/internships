# Outreachy

The Internship Committee should meet and reword these answers if they find it appropriate.

- **Community Name:** GNOME

> The community name you provide will be used to generate unique identifier. The identifier will be used in Outreachy website URLs to reference our community. To ensure old links remain valid, modifying the community name later will not change the community's unique identifier.

- **Community website URL:** https://gnome.org
- **Short description of community:** GNOME is an innovative free software desktop environment that is design-driven and easy to use.

> This should be three sentences for someone who has never heard of your community or the technologies involved. Do not put any links in the short description (use the long description instead).

- **(Optional) Longer description of community.**

[GNOME](https://gnome.org) is an innovative free software desktop that is distributed with many operating systems. It is design-driven and easy to use. GNOME has an exciting and motivated community of people working on projects they feel passionate about and a wide community of users. You can learn more about the latest going-ons in GNOME by reading [Planet GNOME](https://planet.gnome.org] blog aggregator.

> Please avoid adding educational requirements for interns to your community description. Outreachy interns come from a variety of educational backgrounds. Schools around the world may not teach the same topics. If interns need to have specific skills, your mentors need to add application tasks to test those skills.

- **(Optional) Description of your first time contribution tutorial:**

How you make contributions, **will depend on the project idea you are interested in**, so please always consult with the mentor first on what resources you should review and what instructions you should follow for making your first contribution. However, a good starting point is our [Welcome to GNOME platform](https://welcome.gnome.org), where you can learn how to set up the development environment for contributing to many of the GNOME projects.

> If your applicants need to complete a tutorial before working on contributions with mentors, please provide a description and the URL for the tutorial. For example, the Linux kernel asks applicants to complete a tutorial for compiling and installing a custom kernel, and sending in a simple whitespace change patch. Once applicants complete this tutorial, they can start to work with mentors on more complex contributions.